const cluster = require('cluster');
const cpuCount = require('os').cpus().length;

const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const compression = require('compression');
const Sugar = require('sugar');
Sugar.extend();

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const bindPort = 5000;
const bindHost = '0.0.0.0';

const app = express();
app.use(compression());
app.use(bodyParser.json({ limit: '50mb' })); // for parsing application/json
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true })); // for parsing application/x-www-form-urlencoded

if (cluster.isMaster) {
    for (let i = 0; i < cpuCount; i++) {
        cluster.fork();
    }
    cluster.on('exit', worker => {
        cluster.fork();
    });
} else {
    // app.get('/', (request, response) => {
    //     response.send('Hello from Worker ' + cluster.worker.id);
    // });
    app.get('/proxy/*', (req, res) => {
        // var url = req.url.split('/proxy/')[1];
        // if (url.to(4) !== 'http') url = 'http://' + url;
        // var options = {
        //     url: 'https://' + url,
        //     headers: req.headers
        // };
        // req.pipe(request(options)).pipe(res);
        req.pipe(request(req.url.split('/proxy/')[1])).pipe(res);
    });
    app.get('/proxys/*', (req, res) => {
        var url = req.url.split('/proxys/')[1];
        // if (url.to(5) !== 'https') url = 'https://' + url;
        // var options = {
        //     url: 'https://' + url,
        //     headers: req.headers
        // };
        req.pipe(request(req.url.split('/proxys/')[1])).pipe(res);
    });
    app.listen(bindPort, bindHost);
    console.log('Worker %d running! on port %d', cluster.worker.id, bindPort);
}
