const cluster = require('cluster');

const bindPort = 5000;
const bindHost = '127.0.0.1';

const cpuCount = require('os').cpus().length;
const express = require('express');
const app = express();
app.enable('trust proxy');
app.set('trust proxy', 'loopback');

if (cluster.isMaster) {
    for (let i = 0; i < cpuCount; i++) {
        cluster.fork();
    }
    cluster.on('exit', worker => {
        cluster.fork();
    });
} else {
    app.get('/', (request, response) => {
        var ip =
            request.headers['x-forwarded-for'] ||
            request.connection.remoteAddress ||
            request.socket.remoteAddress ||
            request.connection.socket.remoteAddress;
        // console.log(ip);
        console.log(request.headers);
        response.send('Hello from Worker ' + cluster.worker.id);
    });
    app.listen(bindPort, bindHost);
    console.log('Worker %d running! on port %d', cluster.worker.id, bindPort);
}
